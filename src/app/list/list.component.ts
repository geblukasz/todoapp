import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { LoginService } from '../service/login.service';
import { ITask, Task } from './task.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public list: Array<Task> = [];
  public toBeRemovedlist: Array<Task> = [];

  public inputValue: string;
  public priorityValue: number;
  public inputDescription: string;
  public highList: Array<Task> = [];
  public mediumList: Array<Task> = [];
  public lowList: Array<Task> = [];
  public messageFromServer = '';
  public currentTask: Task;

  constructor(private http: HttpClient, public loginService: LoginService) {
    this.currentTask = new Task(0, "", false, 0, "", 0);
  }

  ngOnInit(): void {
    this.getAllValues();
  }

  getAllValues(): void {
    this.http.get<Array<Task>>('http://localhost:8080/task/list').subscribe(
      x => this.list = x
    )
  }

  public handleClick(): void {
    if (this.priorityValue && this.inputValue && this.inputValue.trim().length > 0 && this.priorityValue > 0 && this.priorityValue < 4) {
      this.currentTask = new Task(0, this.inputValue, false, this.priorityValue, this.inputDescription, 0);
      this.list.push(this.currentTask);
      this.http.post<ITask>('http://localhost:8080/task', this.currentTask).subscribe(
        () => this.getAllValues()
      )
    } else {
      this.wrongInputData();
    }
    this.inputValue = "";
    this.priorityValue = undefined;
    this.inputDescription = "";
  }

  public remove(task: Task) {
    const index = this.list.indexOf(task);
    this.list.splice(index, 1);
    this.http.post<ITask>('http://localhost:8080/task/delete', task).subscribe(
      () => console.log("Usunięto zadanie")
    )
  }



  public markAsCompleted(task: Task) {
    alert('Oznaczono zadanie jako wykonane');
    task.completed = true;
    this.http.post<ITask>('http://localhost:8080/task/modification', task).subscribe(
      () => console.log('Zmodyfikowano zadanie')
    )
  }

  public onKeyDownEvent(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      this.handleClick();
    }
  }

  public removeAll() {
    this.http.post<ITask>('http://localhost:8080/task/deleteAll', this.toBeRemovedlist).subscribe(
      () => this.getAllValues()
    )
  }

  public removeCompletedTasks() {
    for(let task of this.list){
      if(task.completed === true){
        this.toBeRemovedlist.push(task);
      }
    }
    this.removeAll()
  }

  public showStatistics() {
    this.highList = [];
    this.mediumList = [];
    this.lowList = [];

    for (let task of this.list) {
      if (!task.completed) {
        if (task.priority === 1) {
          this.highList.push(task);
        }
        else if (task.priority === 2) {
          this.mediumList.push(task);
        }
        else if (task.priority === 3) {
          this.lowList.push(task);
        }
      }
    }
  }


  public isListValid(list: Array<Task>): boolean {
    if (list.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public wrongInputData(): void {

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })


  }

  public editTask(task: Task): void {
    Swal.mixin({
      input: 'text',
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      progressSteps: ['1', '2', '3']
    }).queue([
      {
        title: 'Nazwa',
        text: 'Podaj nową nazwę zadania'
      },
      {
        title: 'Priorytet',
        text: 'Podaj nowy priorytet zadania'
      },
      {
        title: 'Opis',
        text: 'Podaj nowy opis zadania'
      },
    ]).then((result) => {
      // @ts-ignore
      if (result.value) {
        // @ts-ignore
        task.name = result.value[0];
        // @ts-ignore
        task.priority = result.value[1];
        // @ts-ignore
        task.description = result.value[2];

        this.updateTask(task);

        Swal.fire({
          title: 'Zadanie zostało zmienione! :)',
          confirmButtonText: 'Lovely!'
        })
      }
    })
  }


  public updateTask(task: Task) {
    this.http.post<ITask>('http://localhost:8080/task/edit', task).subscribe(
      () => this.getAllValues()
    )
  }

}

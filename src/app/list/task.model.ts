export interface ITask {
    id: number,
    name: string;
    completed: boolean;
    priority: number;
    description: string;
    userId: number;
}

export class Task implements ITask {
    constructor(
        public id: number,
        public name: string,
        public completed: boolean,
        public priority: number,
        public description: string,
        public userId: number
    ){}
}